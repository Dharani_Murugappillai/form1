import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  selectedOption: string='';
  constructor() { }

  ngOnInit(): void {
  }


  setradio(option: string): void   
  {  
  
        this.selectedOption = option;  
          
  }  


  isSelected(name: string): boolean   
  {  
  
        if (!this.selectedOption) {
            return false;  
  }  
  return (this.selectedOption === name);
}
}

